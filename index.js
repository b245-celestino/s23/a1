let trainer = {
  name: "Ash Ketchum",
  age: 10,
  pokemon: ["Pikachu", "Charizard", "Squirtle", "Balbasaur"],
  friends: [{ hoenn: ["May", "Max"] }, { kanto: ["Brock", "Misty"] }],
  talk: function () {
    console.log("Pikachu! I choose you!");
  },
};
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of Bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();

let pikachu = new pokemon("Pikachu", 12, 24, 12);
let geodude = new pokemon("Geodude", 8, 16, 8);
let mewtwo = new pokemon("Mewtwo", 100, 200, 100);
function pokemon(pokemonName, pokemonLevel, pokemonHealth, pokemonAttack) {
  this.name = pokemonName;
  this.level = pokemonLevel;
  this.health = pokemonHealth;
  this.attack = pokemonAttack;
  this.faint = function () {
    console.log(pokemonName + " fainted");
  };
  this.tackle = function (targetPokemon) {
    console.log(this.name + " tackles " + targetPokemon.name);
    let hpAfterTackle = targetPokemon.health - this.attack;

    console.log(
      targetPokemon.name + " health is now reduced to " + hpAfterTackle
    );
    targetPokemon.health = hpAfterTackle;
    console.log(targetPokemon);

    if (targetPokemon.health <= 0) {
      targetPokemon.faint();
    }
  };
}

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);
